resource "aws_security_group" "instance-sg" {
  name   = "sandbox-sg"
  vpc_id = module.sanbox-vpc.vpc_id

  tags = {
    "Name" = "sandbox-instance-sg"
  }
}
resource "aws_security_group_rule" "sandbox-egress" {
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = -1
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.instance-sg.id
}
resource "aws_security_group_rule" "sandbox-ingress-ssh" {
  type        = "ingress"
  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = [ "176.253.163.47/32" ]

  security_group_id = aws_security_group.instance-sg.id
}
resource "aws_security_group_rule" "sandbox-ingress-http" {
  type        = "ingress"
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = [ "176.253.163.47/32" ]

  security_group_id = aws_security_group.instance-sg.id
}
resource "aws_security_group_rule" "sandbox-ingress-https" {
  type        = "ingress"
  from_port   = 443
  to_port     = 443
  protocol    = "tcp"
  cidr_blocks = [ "176.253.163.47/32" ]

  security_group_id = aws_security_group.instance-sg.id
}

