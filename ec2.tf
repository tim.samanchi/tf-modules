resource "aws_instance" "sandbox-Ubuntu" {
    
    count = var.instance-count
    
    ami = var.Ubuntu-ami
    instance_type = var.instance-type

    subnet_id = module.sanbox-vpc.public_subnets[0]
    vpc_security_group_ids = [ aws_security_group.instance-sg.id ]

    key_name = var.ssh-key-name

    connection {
        type = "ssh"
        host = "self.public_ip"
        user        = "ec2-user"
        private_key = file(var.ssh-key-path)
    }

    tags = {
        Name = "Ubuntu-sandbox"
        Environment = var.environment-list[0]
        Function = var.function-map["tr"]
    }
}