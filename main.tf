module "sanbox-vpc" {
    source = "terraform-aws-modules/vpc/aws"
    version = "5.0.0"

    name = "sandbox-frontend-vpc"
    cidr = var.vpc-cidr

    azs = [ "eu-west-1a","eu-west-1b","eu-west-1c" ]
    private_subnets = [ "10.10.1.0/24", "10.10.2.0/24", "10.10.3.0/24" ]
    public_subnets = [ "10.10.101.0/24", "10.10.102.0/24", "10.10.103.0/24" ]

    enable_nat_gateway = true
    single_nat_gateway = true

}